-- Database: buynsell

-- DROP DATABASE buynsell;

CREATE DATABASE buynsell
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Bengali_India.1252'
       LC_CTYPE = 'Bengali_India.1252'
       CONNECTION LIMIT = -1;