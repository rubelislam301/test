SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `amazon` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `amazon` ;

-- -----------------------------------------------------
-- Table `amazon`.`users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `amazon`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(255) NOT NULL ,
  `password` VARCHAR(255) NOT NULL ,
  `displayName` VARCHAR(45) NULL ,
  `DateCreated` DATETIME NOT NULL ,
  `LastUpdated` TIMESTAMP NULL ,
  `user_type` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `amazon`.`product`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `amazon`.`product` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL ,
  `manufacturerID` INT NOT NULL ,
  `manufacturerpartNumber` VARCHAR(45) NULL ,
  `DateCreated` DATETIME NOT NULL ,
  `LastUpdated` TIMESTAMP NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `amazon`.`manufacturers`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `amazon`.`manufacturers` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `url` VARCHAR(255) NOT NULL ,
  `DateCreated` DATETIME NOT NULL ,
  `LastUpdated` TIMESTAMP NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `amazon`.`images`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `amazon`.`images` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `productID` INT NOT NULL ,
  `url` VARCHAR(255) NOT NULL ,
  `DateCreated` DATETIME NOT NULL ,
  `imagescol` TIMESTAMP NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `amazon`.`user_type`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `amazon`.`user_type` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_type` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

USE `amazon` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;




ALTER TABLE `users` ADD constraint foreign key (`user_type`) references `user_type`(`id`);


ALTER TABLE `images` ADD constraint foreign key (`productID`) references `product`(`id`);

ALTER TABLE `product` ADD constraint foreign key (`manufacturerID`) references `manufacturers`(`id`);
